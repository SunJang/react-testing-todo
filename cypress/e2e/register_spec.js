var faker = require("faker");
import { userBuilder } from "../support/generate";
describe("Register", () => {
  it("can register", () => {
    const user = userBuilder();
    cy.visit("/auth/register");
    cy.getByPlaceholderText(/firstname/i)
      .type(user.firstName)
      .getByPlaceholderText(/lastname/i)
      .type(user.lastName)
      .getByPlaceholderText(/email/i)
      .type(user.email)
      .getByPlaceholderText(/password/i)
      .type(user.password)
      .getByText(/register/i)
      .parent()
      .click()
      .url()
      .should("eq", Cypress.config().baseUrl + "/")
      .window()
      .its("localStorage.auth")
      .should("be.a", "string");
  });
});
