import { NotFoundError } from '../../errors';
import { Router, Request, Response, NextFunction } from 'express';

import { getRepository } from 'typeorm';
import { Todo } from '../../entity/Todo';
import { User } from '../../entity/User';
const validator = require('express-joi-validation')({ passError: true });
import { createTodoBodySchema, updateTodoBodySchema, idTodoParamSchema } from './schemas';

var router = Router();

// define the home page route
router.get('/', async (req: Request, res: Response, next: NextFunction) => {
    try {
        const result = await getRepository(Todo).find({ creator: req.user.id });
        res.send(result);
    } catch (e) {
        next(e);
    }
});

router.post(
    '/',
    validator.body(createTodoBodySchema),
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            if (!(req.user instanceof User)) {
                throw new Error('no valid User');
            }
            const { title, done } = req.body;
            const repository = getRepository(Todo);
            const todo = repository.create();

            todo.creator = req.user;
            todo.title = title;
            todo.done = done || false;
            const resp = await repository.save(todo);

            res.send(resp);
        } catch (e) {
            next(e);
        }
    },
);

router.put(
    '/:id',
    validator.params(idTodoParamSchema),
    validator.body(updateTodoBodySchema),
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { id } = req.params;
            const { title, done } = req.body;
            const repository = getRepository(Todo);
            const entities = await repository.find({ id, creator: req.user.id });
            if (!entities.length) {
                throw new NotFoundError();
            }
            const entity = entities[0];
            const updatedEntity = {
                title: title || entity.title,
                done: done !== undefined ? done : entity.done,
            };
            repository.update(id, updatedEntity);

            res.send(updatedEntity);
        } catch (e) {
            next(e);
        }
    },
);

router.delete(
    '/:id',
    validator.params(idTodoParamSchema),
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { id } = req.params;
            const repository = getRepository(Todo);
            const entity = await repository.findOne({ id, creator: req.user.id });
            if (!entity) {
                throw new NotFoundError();
            }
            const resp = await repository.delete(id);

            res.send({ id });
        } catch (e) {
            next(e);
        }
    },
);

export default router;
