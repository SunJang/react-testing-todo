# GraphQL Starter

## License

MIT

## Author

Tim Kolberger
[<img src="https://simpleicons.org/icons/gmail.svg" width="24"/>](mailto:tim@kolberger.eu)
[<img src="https://simpleicons.org/icons/twitter.svg" width="24"/>](https://twitter.com/TimKolberger)
