// @flow
import { type Dispatch } from 'redux'
import axios from 'axios'
import { notify } from 'reapop'

import { urls } from '../../urls'
import { authenticate, logout as logoutAuth } from '../../auth'

export const actions = {
  AUTH_LOGIN_START: 'AUTH_LOGIN_START',
  AUTH_LOGIN_SUCCESS: 'AUTH_LOGIN_SUCCESS',
  AUTH_LOGIN_ERROR: 'AUTH_LOGIN_ERROR',
  AUTH_LOGOUT_START: 'AUTH_LOGOUT_START',
  AUTH_LOGOUT_SUCCESS: 'AUTH_LOGOUT_SUCCESS',
  AUTH_LOGOUT_ERROR: 'AUTH_LOGOUT_ERROR',
  AUTH_REGISTER_START: 'AUTH_REGISTER_START',
  AUTH_REGISTER_SUCCESS: 'AUTH_REGISTER_SUCCESS',
  AUTH_REGISTER_ERROR: 'AUTH_REGISTER_ERROR',
}

export const login = (email: string, password: string) => async (
  dispatch: Dispatch,
) => {
  dispatch({
    type: actions.AUTH_LOGIN_START,
  })
  axios
    .post(urls.api.auth.login, { email, password })
    .then(async payload => {
      await authenticate(payload.data.token)
      dispatch({
        type: actions.AUTH_LOGIN_SUCCESS,
        payload,
      })

      return true
    })
    .catch(e => {
      dispatch({
        type: actions.AUTH_LOGIN_ERROR,
        error: e,
      })

      const { response } = e
      dispatch(
        notify({ message: response.data.detail, status: response.status }),
      )
      return false
    })
}

export const logout = () => async (dispatch: Dispatch) => {
  dispatch({
    type: actions.AUTH_LOGOUT_START,
  })
  try {
    logoutAuth()
    dispatch({
      type: actions.AUTH_LOGOUT_SUCCESS,
    })
    return true
  } catch (e) {
    dispatch({
      type: actions.AUTH_LOGOUT_ERROR,
      error: e,
    })
    dispatch(notify({ message: e.message, status: 'error' }))
    return false
  }
}

export const register = (
  email: string,
  password: string,
  firstName: string,
  lastName: string,
) => (dispatch: Dispatch) => {
  dispatch({
    type: actions.AUTH_REGISTER_START,
  })
  axios
    .post(urls.api.auth.register, {
      email,
      password,
      firstName,
      lastName,
    })
    .then(async payload => {
      await authenticate(payload.data.token)
      dispatch({
        type: actions.AUTH_REGISTER_SUCCESS,
        payload,
      })
    })
    .catch(e => {
      dispatch({
        type: actions.AUTH_REGISTER_ERROR,
        error: e,
      })

      const { response } = e
      dispatch(
        notify({ message: response.data.detail, status: response.status }),
      )
    })
}
