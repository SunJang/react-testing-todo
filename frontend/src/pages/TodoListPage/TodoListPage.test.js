// @flow

// eslint-disable-next-line
import React from 'react'
import 'jest-dom/extend-expect'
import mockAxios from 'axios'
import { fireEvent, cleanup, waitForElement } from 'react-testing-library'
import { render } from '../../test-utils'

import { TodoListPage } from './index'
import { Todo } from '../../types'

afterEach(cleanup)

describe(`Connected Todo List`, () => {
  test('can render with state Todo with defaults', () => {
    // Arrange
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({
        data: [],
      }),
    )
    const { queryAllByTestId } = render(<TodoListPage />)

    // Assert

    expect(queryAllByTestId('list-item')).toHaveLength(0)
    expect(mockAxios.post).toHaveBeenCalledTimes(1)
  })

  test('can add Todo to List', async () => {
    // Arrange
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({
        data: [],
      }),
    )
    mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve({
        data: [new Todo('test')],
      }),
    )

    const { getByPlaceholderText, getAllByTestId } = render(<TodoListPage />)

    // Actf
    const input = getByPlaceholderText(/done/i)
    fireEvent.change(input, { target: { value: 'new Todo' } })
    fireEvent(
      input,
      new KeyboardEvent('keydown', {
        key: 'Enter',
        keyCode: 13,
        which: 13,
        bubbles: true,
      }),
    )

    // Assert
    expect(mockAxios.post).toHaveBeenCalledTimes(1)
    const listItems = await waitForElement(() => getAllByTestId('list-item'))

    expect(listItems).toHaveLength(1)
  })

  test('can remove Todo from List', async () => {
    // Arrange
    const initialTodo = new Todo('test')
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({
        data: [initialTodo],
      }),
    )
    mockAxios.delete.mockImplementationOnce(() =>
      Promise.resolve({
        data: { id: initialTodo.id },
      }),
    )
    const { getByTestId, queryAllByTestId, queryByText } = render(
      <TodoListPage />,
    )

    // Act
    const firstTodo = getByTestId('list-item')
    fireEvent.click(firstTodo.querySelector('.anticon-close-circle'), {})

    const noData = await waitForElement(() => queryByText(/no.+data/i))
    const listItems = await waitForElement(() => queryAllByTestId('list-item'))

    // Assert
    expect(noData).toBeDefined()
    expect(listItems).toHaveLength(0)
  })

  test('can edit Todo', async () => {
    const initialTodo = new Todo('test')
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({
        data: [initialTodo],
      }),
    )
    mockAxios.put.mockImplementationOnce(() =>
      Promise.resolve({
        data: { ...initialTodo, title: 'changed Todo' },
      }),
    )
    const { getByTestId, getAllByTestId, queryByText } = render(
      <TodoListPage />,
    )

    expect(
      await waitForElement(() => getAllByTestId('list-item')),
    ).toHaveLength(1)
    expect(queryByText('changed Todo')).toBeNull()
    const firstTodo = getByTestId('list-item')
    fireEvent.click(getByTestId('edit-todo'), {})
    const editInput = firstTodo.querySelector('input')
    fireEvent.change(editInput, {
      target: { value: 'changed Todo' },
    })
    fireEvent(
      editInput,
      new KeyboardEvent('keydown', {
        key: 'Enter',
        keyCode: 13,
        which: 13,
        bubbles: true,
      }),
    )
    expect(
      await waitForElement(() => queryByText('changed Todo')),
    ).toBeDefined()
  })

  test.only('can toggle Todo', async () => {
    const initialTodo = new Todo('test')
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({
        data: [initialTodo],
      }),
    )
    mockAxios.put.mockImplementationOnce(() =>
      Promise.resolve({
        data: { ...initialTodo, done: true },
      }),
    )
    const { getByTestId, getAllByTestId } = render(<TodoListPage />)
    expect(
      await waitForElement(() => getAllByTestId('list-item')),
    ).toHaveLength(1)

    expect(getByTestId('checkbox')).toHaveAttribute('value', 'false')
    fireEvent.click(getByTestId('checkbox'))
    expect(await waitForElement(() => getByTestId('checkbox'))).toHaveAttribute(
      'value',
      'true',
    )
  })
})
