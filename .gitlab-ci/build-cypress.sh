#!/usr/bin/env sh

set -ex

docker pull $IMAGE_TAG_CYPRESS:$CI_COMMIT_REF_SLUG || true
docker pull $IMAGE_TAG_CYPRESS:develop || true

docker build \
    --cache-from $IMAGE_TAG_CYPRESS:$CI_COMMIT_REF_SLUG \
    --cache-from $IMAGE_TAG_CYPRESS:develop \
    -t $IMAGE_TAG_CYPRESS:$CI_COMMIT_REF_SLUG \
    -f .docker/cypress/Dockerfile \
    cypress

docker push $IMAGE_TAG_CYPRESS:$CI_COMMIT_REF_SLUG
