#!/usr/bin/env sh

set -ex

IMAGE_TAG_FRONTEND_BUILDER="${IMAGE_TAG_FRONTEND}_builder"

docker pull $IMAGE_TAG_FRONTEND_BUILDER:$CI_COMMIT_REF_SLUG || true
docker pull $IMAGE_TAG_FRONTEND_BUILDER:develop || true

docker pull $IMAGE_TAG_FRONTEND:$CI_COMMIT_REF_SLUG || true
docker pull $IMAGE_TAG_FRONTEND:develop || true

docker build \
    --cache-from $IMAGE_TAG_FRONTEND_BUILDER:$CI_COMMIT_REF_SLUG \
    --cache-from $IMAGE_TAG_FRONTEND_BUILDER:develop \
    -t $IMAGE_TAG_FRONTEND_BUILDER:$CI_COMMIT_REF_SLUG \
    -f .docker/nginx/Dockerfile_builder \
    .

docker build \
    --cache-from $IMAGE_TAG_FRONTEND:$CI_COMMIT_REF_SLUG \
    --cache-from $IMAGE_TAG_FRONTEND:develop \
    -t $IMAGE_TAG_FRONTEND:$CI_COMMIT_REF_SLUG \
    -f .docker/nginx/Dockerfile \
    --build-arg builder="$IMAGE_TAG_FRONTEND_BUILDER:$CI_COMMIT_REF_SLUG" \
    .

docker push $IMAGE_TAG_FRONTEND_BUILDER:$CI_COMMIT_REF_SLUG
docker push $IMAGE_TAG_FRONTEND:$CI_COMMIT_REF_SLUG
